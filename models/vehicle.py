from lxml import etree

from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval


class fleet_vehicle(models.Model):
    _inherit = 'fleet.vehicle'



    def _generate_virtual_location(self, cr, uid, truck, vehicle_ok, trailer_ok, context): 
        pass
    
    
    # def onchange_basic_odometer(self, cr, uid, ids, counter_basic, context=None):
    #     data={}
    #     if counter_basic:
    #         data['odometer'] = counter_basic
    #     return {'value' : data}

    @api.constrains('license_plate')
    def check_license_plate_format(self):
        for rec in self:

            phoneNumRegex = re.compile(r'[A-Z]{2}[-][0-9]{1,2}[-]([A-Z]{1,2}[-][0-9]{4})')
            plate = re.compile(r'[A-Z]{2}[-][0-9]{1,2}[-][0-9]+}')
            if rec.vehicle_ok or rec.rent_vehicle:
                mo = phoneNumRegex.search(rec.license_plate)
                m = plate.search(rec.license_plate)
                if not mo :
                    raise except_orm(_('Warning'),
                                     _('Please Enter REG No in this format KL-01-XX-XXXX or KL-01-X-XXXX'))




    is_rent_mach = fields.Boolean(string="Is Rent Machinery")
    gasoil_id = fields.Many2one('product.product', 'Fuel', required=False, domain=[('fuel_ok','=','True')])
    emi_no = fields.Char('EMI No', size=64)
    bank_id = fields.Many2one('res.bank','Bank Details')
    emi_lines =  fields.One2many('bank.emi.lines','emi_line', 'EMI Payment Details')
    emi_start_date = fields.Date('Start Date')
    last_paid_date = fields.Date('Last Date Paid')
    next_payment_date = fields.Date('Next Payment Date')
    total_due = fields.Float('Total Due', Default=0.0)
    total_paid = fields.Float('Total Paid', Default=0.0)
    balance_due = fields.Float('Balance', Default=0.0)    
    ############ insurance Details
    ins_no = fields.Char('EMI No', size=64)
    agent_id = fields.Many2one('res.partner','Agent Details')
    ins_lines = fields.One2many('agent.ins.lines','ins_line', 'Insurance Payment Details')
    last_paid_date_ins = fields.Date('Last Date Paid')
    next_payment_date_ins = fields.Date('Next Payment Date')  
    puc_lines = fields.One2many('puc.lines','puc_line', 'PUC Details')
    vehilce_old_odometer = fields.Float('Vehicle Old Old OdoMeter', readonly=False)     
    mileage = fields.Float('Current Reading',)
    odometer = fields.Float( string='Last Odometer', help='Odometer measure of the vehicle at this moment')
    rate_per_km = fields.Float('Rate Per Km')
    vehicle_under  =fields.Many2one('res.partner','Vehicle Under')
    per_day_rent = fields.Float('Rent Per Day')
    rent_vehicle = fields.Boolean(default=False)
    machinery = fields.Boolean(default=False)
    brand_id = fields.Many2one('fleet.vehicle.model.brand', 'Brand')
    vehicle_ok = fields.Boolean('Vehicle')
    model_id = fields.Many2one('fleet.vehicle.model', 'Model', required=False)
    name = fields.Char(compute="_get_tms_vehicle_name", string='Nom', store=True)
    capacity = fields.Float('Capacity')
    chase_no = fields.Char("Chase No")
    engine_no = fields.Char("Engine No")
    sl_no = fields.Char("Sl No")
    vehicle_categ_id = fields.Many2one('vehicle.category.type', string="Vehicle Type")
    expected_working = fields.Float("Expected Working Hour")
    tanker_bool = fields.Boolean("Tanker",default=False)
    fleet_receipt_details_ids = fields.One2many('fleet.receipt.details','name',"Fleet Receipt Details")
    fleet_issue_details_ids = fields.One2many('fleet.issue.details', 'name', "Fleet Issue Details")
    location_id = fields.Many2one('stock.location',"Tanker Location")
    permit_date = fields.Date("Permit Date")
    insurance_date = fields.Date("Insurance Date")
    road_tax_date = fields.Date("Road Tax Date")
    fitness_date = fields.Date("Fitness Date")
    pollution_date = fields.Date("Pollution Date")
    fleet_no = fields.Char("Fleet No")
    insured_by = fields.Many2one('vehicle.insurer',"Insured By")
    premium_amt = fields.Float("Premium Amount")
    state = fields.Selection([('draft','Draft'),('confirm','Confirm'),
                              ('approved','Authorized')],default='draft')
    year_of_manu = fields.Char("Year of Manufactuer")
    vehicle_owner = fields.Many2one('vehicle.owner',"Vehicle Owner")
    vehicle_rate_per_km_ids = fields.One2many('vehicle.rate.per.km','vehicle_id',"Vehicle Rate Per Km Details")
    purchase_value = fields.Float("Purchase Value")
    other = fields.Boolean("Other")
    rent_other = fields.Boolean("Rent Other")

    @api.multi
    def action_confirm(self):
        for rec in self:
            rec.state = 'confirm'

    @api.multi
    def action_approve(self):
        for rec in self:
            rec.state = 'approved'




    @api.depends('license_plate')
    def _get_tms_vehicle_name(self):
        for record in self:

            record.name = record.license_plate




    @api.constrains('name')
    def _check_duplicate_name(self):
        names = self.search([])
        for c in names:
            if self.id != c.id:
                if self.name and c.name:
                    if self.name.lower() == c.name.lower() or self.name.lower().replace(" ", "") == c.name.lower().replace(" ", ""):
                        raise osv.except_osv(_('Error!'), _('Error: vehicle name must be unique'))
            else:
                pass


class bank_emi_lines(models.Model):
    _name = 'bank.emi.lines'
    
    
    name = fields.Char('Name', size=64)
    date = fields.Date('Date')
    payment_mode = fields.Selection([('cash','Cash'),('bank','Bank'),('cheque','Cheque')], 'Mode Of Payment', select=True)
    reference = fields.Text('Reference')
    amount = fields.Float('Amount', Default=0.0)
    emi_line = fields.Many2one('fleet.vehicle', 'EMI Payment Details')
    receipt_no = fields.Char('Receipt No', size=64)


class bank_emi_lines(models.Model):
    _name = 'agent.ins.lines'
    
    
    name = fields.Char('Name', size=64)
    date = fields.Date('Date')
    payment_mode = fields.Selection([('cash','Cash'),('bank','Bank'),('cheque','Cheque')], 'Mode Of Payment', select=True)
    reference = fields.Text('Reference')
    amount = fields.Float('Amount', Default=0.0)
    ins_line = fields.Many2one('fleet.vehicle', 'Insurance Payment Details')
    receipt_no = fields.Char('Receipt No', size=64)


class puc_lines(models.Model):
    _name = 'puc.lines'
    
    
    name = fields.Char('Name', size=64)
    date = fields.Date('Date')
    exp_date = fields.Date('Expiry Date')
    reference = fields.Text('Reference')
    amount = fields.Float('Amount', Default=0.0)
    puc_line = fields.Many2one('fleet.vehicle', 'Insurance Payment Details')
    receipt_no = fields.Char('Receipt No', size=64)

class FleetIssueDetails(models.Model):
    _name = 'fleet.issue.details'
    
    name = fields.Many2one('fleet.vehicle',"Vehicle")
    date = fields.Date("Date")
    mrn_no = fields.Many2one('material.issue.slip',"MRN No")
    item_id = fields.Many2one('product.product',"Item")
    qty = fields.Float("Qty")
    rate = fields.Float("Rate")
    amount = fields.Float("Amount")

class FleetReceiptDetails(models.Model):
    _name = 'fleet.receipt.details'
    
    name = fields.Many2one('fleet.vehicle', "Vehicle")
    date = fields.Date("Date")
    grr_no = fields.Many2one('goods.recieve.report', "GRR No")
    item_id = fields.Many2one('product.product', "Item")
    qty = fields.Float("Qty")
    rate = fields.Float("Rate")
    tax_ids = fields.Many2many('account.tax','fleet_receipt_tax_rel','fleet_receipt_id','tax_id',string="Taxes")
    amount = fields.Float("Amount")



class VehicleRatePerKm(models.Model):
    _name='vehicle.rate.per.km'

    rate = fields.Float("Rate")
    with_efect = fields.Date("With Effect From")
    vehicle_id = fields.Many2one('fleet.vehicle',"Vehicle")