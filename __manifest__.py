{
    'name': 'TMS Extension 10',
    'version': '1.0',
    'category': 'Tools',
    'description': """
The module adds the possibility to display data from OpenERP in Google Spreadsheets in real time.
=================================================================================================
""",
    'author': 'Hiworth',
    'website': 'http://www.hiworthsolutions.com',
    'depends': ['fleet','stock'],
    'data' : [
        'report/vehicle_maintenace.xml',
        'views/hiworth_fleet.xml',
        'views/vehicle.xml'
    ],
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: